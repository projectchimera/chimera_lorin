/*
 * main.cpp
 *
 *  Created on: Mar 9, 2017
 *      Author: lorin
 */

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <fstream>
#include "populationGen.h"
#include "gtest/gtest.h"


int main(int argc, char* argv[]){
	//std::cout << "count: " << argc << ", " << argv[0] << std::endl;
	std::string filename = "src/GeoverdelingsProfiel.xml";
	std::string test = "test";
	if(argc > 1 && argv[1] != test){
		//std::cout << "argv2: " << argv[2] << std::endl;
		//std::cout << "one true"<< std::endl;
		filename = "src/";
		filename.append(argv[1]);
	}
	else if((argc > 2 && argv[2] == test) || (argc > 1 && argv[1] == test)){
		std::cout << "two true"<< std::endl;
		//std::cout<< "hih?" << std::endl;
		::testing::InitGoogleTest(&argc, argv);
		return RUN_ALL_TESTS();

	}
	//std::cout << "filename: " << argv[1] << std::endl;

	PopulationGenerator Popgen =PopulationGenerator(filename);
	if(Popgen.generate("otherplaceholder")){
		return 0;
	};
	return -1;


}


