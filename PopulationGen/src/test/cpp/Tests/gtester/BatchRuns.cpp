/*
\ *  This is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2017, Willem L, Kuylen E, Stijven S & Broeckhove J
 */

/**
 * @file
 * Implementation of scenario tests running in batch mode.
 */
#include "gtest/gtest.h"
#include "populationGen.h"
#include <ctime>
#include <cstdlib>

std::vector<float> prob;
std::vector<unsigned int> id;




TEST (AliasTest, CorrectID) {
	srand(time(NULL));
	for(int i = 0; i < 20; i++){
		prob.push_back(1/20-1/200+(1/20)/(i+1));
		id.push_back(i);

	};
	alias testAlias = alias(prob, id);
    EXPECT_TRUE(testAlias.generate(rand(), rand()) <20);
}

TEST (ParserTest, CorrectParse) {
	srand(time(NULL));
	geoParser geoParser("src/GeoverdelingsProfielTest.xml");
	float maxLatitude = geoParser.maxLatitude;//60
	float minLatitude = geoParser.minLatitude; //40
	float maxLongitude = geoParser.maxLongitude;//20
	float minLongitude = geoParser.minLongitude;//0
	float nrVillages = geoParser.nrVillages;//10
    EXPECT_TRUE(maxLongitude == 20 && maxLatitude == 60 && minLongitude == 0 && minLatitude == 40 && nrVillages == 10);
}

TEST (scenarioTest, CorrectExercution) {
	PopulationGenerator Popgen =PopulationGenerator("src/GeoverdelingsProfielTest.xml");
	bool testresult = Popgen.generate("placeholder");
	EXPECT_TRUE(testresult);
}

/*

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}*/
 //end-of-namespace-Tests


