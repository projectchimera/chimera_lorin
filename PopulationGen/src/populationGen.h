/*
 * populationGen.h
 *
 *  Created on: Mar 9, 2017
 *      Author: lorin
 */
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <vector>
#ifndef SRC_POPULATIONGEN_H_
#define SRC_POPULATIONGEN_H_



struct school{
public:
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
};

struct college{
public:
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int maxSize;
	unsigned int cityId;
	unsigned int id;
};

struct workplace{
public:
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
};

struct community{
public:
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int maxSize;
	unsigned int id;

};
struct city{
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
	std::vector<college*> chosenColleges;
	std::vector<school*> chosenSchools;
	std::vector<workplace*> chosenWorkplaces;
	std::vector<community*> chosenCommunities;
};

struct village{
	float latitude;
	float longitude;
	unsigned int size;
	unsigned int id;
	std::vector<college*> chosenColleges;
	std::vector<school*> chosenSchools;
	std::vector<workplace*> chosenWorkplaces;
	std::vector<community*> chosenCommunities;
};
class alias{
public:
	alias();
	alias(std::vector<float> probabilities, std::vector<unsigned int> ids);
	int generate(float randomN, float aliasRandom); //returns the alias
private:
	int size;
	std::vector< int > Alias;
	std::vector< int > Original;
	std::vector<float> Prob;
};

class PopulationGenerator{
public:
	PopulationGenerator(std::string configFile);
	bool generate(std::string filenameout);

private:
	std::vector<community*> communities;
	std::vector<school*> schools;
	std::vector<college*> colleges;
	std::vector<workplace*> workplaces;
	std::vector<village*> villages;
	std::vector<city*> cities;
	void* randGen;
	int popSize;
	float totalcitypop;
	alias AliasmethodCities;
	float CoordToMeters;

};

class Person{
public:
	Person(int household, int ageIn, int work, int school, int CommunityIDin);
	void WriteToFile(std::ofstream* filenameout);
private:
	int age;
	int WorkID;
	int SchoolID;
	int HouseholdID;
	int CommunityID;


};

class randGen{
public:
	float getNum(float upper, float lower, int precision);
	randGen(std::string generator, double seed);

};





class geoParser{
public:
	geoParser(std::string fileName);
	void parseFile();
	float minLongitude;
	float maxLongitude;
	float minLatitude;
	float maxLatitude;
	int nrVillages;
	int pop_Size;
	bool coords;
	std::vector<city*> cities;
private:

	std::string fileName;



};

bool inRadius(float radiusKm, float CoordtoMeters, float longitudeHouse, float latitudeHouse,  float longitude, float latitude, bool coords);

class householdParser{
public:
	householdParser(std::string filename);
	std::vector<int> getHousehold(int seed);
private:
	std::vector< std::vector<int> > Households;
};



class populationParser{

public:
	populationParser(std::string filename);
	void parseFile();
};








#endif /* SRC_POPULATIONGEN_H_ */
